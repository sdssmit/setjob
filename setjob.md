 self.setJob = function(name, grade)

    local lastJob = json.decode(json.encode(self.job))
    
    MySQL.Async.fetchAll(
      'SELECT * FROM whitelist WHERE identifier = @id2',
      {
        ['@id2'] = self.identifier
      },
      function(result2)
	  for i=1, #name do
			
			self.job['name']  = result2[i].name
			self.job['grade']  = result2[i].grade
			
            if(result2[i].name == name) then
			    GradeToUse = result2[i].grade
            else
                GradeToUse = 0
            end
             print("WHITELISTED JOB:", result2[i].name)
            print("WHITELISTED GRADE:", result2[i].grade)
      end
	  end
     
    )
    
    MySQL.Async.fetchAll(
      'SELECT * FROM `jobs` WHERE `name` = @name',
      {
        ['@name'] = name
      },
      function(result)

        self.job['id']    = result[1].id
        self.job['name']  = result[1].name
        self.job['label'] = result[1].label

        MySQL.Async.fetchAll(
          'SELECT * FROM `job_grades` WHERE `job_name` = @job_name AND `grade` = @grade',
          {
            ['@job_name'] = name,
            ['@grade']    = GradeToUse
          },
          function(result)

            self.job['grade']        = GradeToUse
            self.job['grade_name']   = result[1].name
            self.job['grade_label']  = result[1].label
            self.job['grade_salary'] = result[1].salary

            self.job['skin_male']    = nil
            self.job['skin_female']  = nil

            if result[1].skin_male ~= nil then
              self.job['skin_male'] = json.decode(result[1].skin_male)
            end

            if result[1].skin_female ~= nil then
              self.job['skin_female'] = json.decode(result[1].skin_female)
            end

            TriggerEvent("esx:setJob", self.source, self.job, lastJob)
            TriggerClientEvent("esx:setJob", self.source, self.job)

          end
        )
       print("NEW JOB:", result[1].name)
        print("ACTUAL SELECTED GRADE:", GradeToUse)
      
      end
    )

  end